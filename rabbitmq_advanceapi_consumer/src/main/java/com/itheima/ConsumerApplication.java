package com.itheima;

import org.springframework.amqp.core.Queue;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @Author: taku
 * @CreateTime: 2020-04-27 16:24
 * @Description: 起步引导类
 */
@SpringBootApplication
public class ConsumerApplication {

    public static void main(String[] args) {
        SpringApplication.run(ConsumerApplication.class, args);
    }

    // 注入队列
    @Bean
    public Queue dirQueue(){
        return new Queue("dirQueue_spring");
    }
}

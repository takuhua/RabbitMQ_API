package com.itheima.listener;

import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * @Author: taku
 * @CreateTime: 2020-04-26 22:33
 * @Description: 消息监听器
 */
@Component
public class MessageListener {

    /**
     * 监听某个队列的消息
     * @param msg 接收到的消息
     */
    // 监听是需要输入监听的队列名字
    @RabbitListener(queues = {"topic_Queue","fanout_Queue_spring","direct_Queue_Spring"})
    public void topicListener(String msg){
        System.out.println("接收到消息：" + msg);
    }

}

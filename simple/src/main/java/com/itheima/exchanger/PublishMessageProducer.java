package com.itheima.exchanger;

import com.itheima.RabbitUtils;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @Author: taku
 * @CreateTime: 2020-04-26 17:39
 * @Description: 消息生产者
 */
public class PublishMessageProducer {
    public static void main(String[] args) {
        try {
            Connection connection = RabbitUtils.getConnection();
            // 创建channel
            Channel channel = connection.createChannel();
            // 声明交换机,订阅式声明,交换机类型
            channel.exchangeDeclare("fanout_exchanger", BuiltinExchangeType.FANOUT);
            String message;
            for (int i = 0; i < 10; i++) {
                message="第" + i + "条信息,来自消息队列: fanout_exchanger";
                channel.basicPublish("fanout_exchanger","",null, message.getBytes("UTF-8"));
            }
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

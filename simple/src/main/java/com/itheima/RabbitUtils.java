package com.itheima;

import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

/**
 * @Author: taku
 * @CreateTime: 2020-04-26 17:33
 * @Description: RAbbitMQ工具类,用于创建连接
 */
public class RabbitUtils {
    public static Connection getConnection() {
        try {
            // 创建工厂
            ConnectionFactory connectionFactory = new ConnectionFactory();
            // 设置密码,用户,端口,地址,虚拟机
            connectionFactory.setVirtualHost("itheima");
            connectionFactory.setHost("localhost");
            connectionFactory.setPort(5672);
            connectionFactory.setUsername("admin");
            connectionFactory.setPassword("admin");
            // 创建连接
            return connectionFactory.newConnection();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}

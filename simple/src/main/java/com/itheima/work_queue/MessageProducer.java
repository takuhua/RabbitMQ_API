package com.itheima.work_queue;

import com.itheima.RabbitUtils;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import sun.security.jgss.HttpCaller;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

/**
 * @Author: taku
 * @CreateTime: 2020-04-26 17:39
 * @Description: 消息生产者
 */
public class MessageProducer {
    public static void main(String[] args) {
        try {
            Connection connection = RabbitUtils.getConnection();
            // 创建channel
            Channel channel = connection.createChannel();
            // 声明队列
            channel.queueDeclare("work_queue",true,false,false,null);
            String message;
            for (int i = 0; i < 10; i++) {
                message="第" + i + "条信息,来自消息队列: work_queue";
                channel.basicPublish("","work_queue",null,message.getBytes());
            }
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

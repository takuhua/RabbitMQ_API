package com.itheima.topic;

import com.itheima.RabbitUtils;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @Author: taku
 * @CreateTime: 2020-04-26 17:39
 * @Description: 消息生产者
 */
public class RoutingMessageProducer {
    public static void main(String[] args) {
        try {
            Connection connection = RabbitUtils.getConnection();
            // 创建channel
            Channel channel = connection.createChannel();
            // 声明交换机,订阅式声明,交换机类型
            channel.exchangeDeclare("topic_exchanger", BuiltinExchangeType.TOPIC);
            String message;
            String routingKey ="";
            for (int i = 0; i < 10; i++) {
                message="第" + i + "条信息,来自消息队列: topic_exchanger";
                if (i % 4 == 0) {
                    routingKey = "log.info";
                }
                if (i % 4 == 1) {
                    routingKey = "log.error";
                }
                if (i % 4 == 2) {
                    routingKey = "warning.log";
                }
                if (i % 4 == 3) {
                    routingKey = "mess.warning.hah";
                }

                channel.basicPublish("topic_exchanger",routingKey,null, message.getBytes("UTF-8"));
            }
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

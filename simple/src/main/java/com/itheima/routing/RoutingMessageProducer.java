package com.itheima.routing;

import com.itheima.RabbitUtils;
import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;

/**
 * @Author: taku
 * @CreateTime: 2020-04-26 17:39
 * @Description: 消息生产者
 */
public class RoutingMessageProducer {
    public static void main(String[] args) {
        try {
            Connection connection = RabbitUtils.getConnection();
            // 创建channel
            Channel channel = connection.createChannel();
            // 声明交换机,订阅式声明,交换机类型
            channel.exchangeDeclare("routing_exchanger", BuiltinExchangeType.DIRECT);
            String message;
            String routingKey ="";
            for (int i = 0; i < 10; i++) {
                message="第" + i + "条信息,来自消息队列: routing_exchanger";
                if (i % 3 == 0) {
                    routingKey = "info";
                }
                if (i % 3 == 1) {
                    routingKey = "error";
                }
                if (i % 3 == 2) {
                    routingKey = "warning";
                }

                channel.basicPublish("routing_exchanger",routingKey,null, message.getBytes("UTF-8"));
            }
            channel.close();
            connection.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

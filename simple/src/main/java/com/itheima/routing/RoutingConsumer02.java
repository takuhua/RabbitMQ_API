package com.itheima.routing;

import com.itheima.RabbitUtils;
import com.rabbitmq.client.*;

import java.io.IOException;

/**
 * @Author: taku
 * @CreateTime: 2020-04-26 17:47
 * @Description: 消息消费者
 */
public class RoutingConsumer02 {
    public static void main(String[] args) {
        try {
            Connection connection = RabbitUtils.getConnection();
            Channel channel = connection.createChannel();
            channel.queueDeclare("routing_queue1",true,false,false,null);
            channel.queueBind("routing_queue1","routing_exchanger","info");
            channel.queueBind("routing_queue1","routing_exchanger","warning");
            // 创建消费者
            DefaultConsumer defaultConsumer = new DefaultConsumer(channel) {

                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
                    String routingKey = envelope.getRoutingKey();
                    String exchange = envelope.getExchange();
                    String message = new String(body);
                    System.out.println(message +
                            "   routingKey:" + routingKey +
                            "   exchanger: " + exchange);

                }
            };
            channel.basicConsume("routing_queue1",defaultConsumer);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}

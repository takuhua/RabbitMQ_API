package com.itheima.callback_method;

import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * @Author: taku
 * @CreateTime: 2020-04-27 17:06
 * @Description: 队列消息接收函调函数式
 */
@Component
public class MyReturnSMethods implements RabbitTemplate.ReturnCallback {


    /**
     * @param message 消息体,可通过message 获取对应的接收状态
     * @param replyCode 错误码
     * @param replyText 错误响应文本
     * @param exchange 交换机
     * @param routingKey 路由key
     * 进入此方法必然是出现错误
     */
    @Override
    public void returnedMessage(Message message, int replyCode, String replyText, String exchange, String routingKey) {
        System.out.println("消息接收失败,请确认后重试!");
        System.out.println("消息内容是: " + new String(message.getBody())) ;
        System.out.println("错误码是: " + replyCode) ;
        System.out.println("错误响应文本是: " + replyText) ;
        System.out.println("交换机是: " + exchange) ;
        System.out.println("路由key是: " + routingKey) ;

    }
}

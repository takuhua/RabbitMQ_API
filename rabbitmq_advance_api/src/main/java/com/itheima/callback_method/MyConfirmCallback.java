package com.itheima.callback_method;

import org.springframework.amqp.rabbit.connection.CorrelationData;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

/**
 * @Author: taku
 * @CreateTime: 2020-04-27 16:39
 * @Description: 交换机的回调函数
 */
@Component
public class MyConfirmCallback implements RabbitTemplate.ConfirmCallback {


    /**
     * @param correlationData 消息信息的集合
     * @param ack 表示交换机是否接收到消息
     * @param cause 表示错误具体的原因,正常是为null
     * 重写confirm方法
     */

    @Override
    public void confirm(CorrelationData correlationData, boolean ack, String cause) {
        // 收到消息的处理
        if (ack) {
            System.out.println("交换机已收到消息");
        }
        // 没有收到消息处理
        if (!ack) {
            System.out.println("交换机没有收到消息,原因是: " + cause);
        }
    }
}

package com.itheima.controller;

import com.itheima.callback_method.MyConfirmCallback;
import com.itheima.callback_method.MyReturnSMethods;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;

/**
 * @Author: taku
 * @CreateTime: 2020-04-27 16:44
 * @Description: 回调函数测试类
 */
@RestController
@RequestMapping("/call")
public class CallbackController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    // 需要注入回调函数
    @Autowired
    private MyConfirmCallback myConfirmCallback;

    // 注入队列回调函数
    @Autowired
    private MyReturnSMethods myReturnSMethods;

    @RequestMapping("/message")
    public String callbackTest() {
        // 设置回调函数
        rabbitTemplate.setConfirmCallback(myConfirmCallback);
        // 向交换机发送信息
        rabbitTemplate.convertAndSend("dirExchange_spring","","你有一条来自国宝的信息,请查收");
        return "消息已发送,请留意查收!"+ new Date();
    }


    @RequestMapping("/return")
    public String myretunns() {
        // 设置队列信息接收回调函数
        rabbitTemplate.setReturnCallback(myReturnSMethods);
        // 向交换机发送信息
//        rabbitTemplate.convertAndSend("dirExchange_spring","withRoutingkey","你有一条来自国宝的信息,请查收");
        rabbitTemplate.convertAndSend("dirExchange_spring","","你有一条来自国宝的信息,需要return,请查收");
        return "消息已发送,请留意查收!"+ new Date();
    }

    @RequestMapping("/both")
    public String both() {
        // 设置队列信息接收回调函数
        rabbitTemplate.setConfirmCallback(myConfirmCallback);
        rabbitTemplate.setReturnCallback(myReturnSMethods);
        // 向交换机发送信息
//        rabbitTemplate.convertAndSend("dirExchange_spring","withRoutingkey","你有一条来自国宝的信息,请查收");
        rabbitTemplate.convertAndSend("dirExchange_spring","","你有一条来自国宝的信息,需要return,请查收");
        return "消息已发送,请留意查收!"+ new Date();
    }

    @RequestMapping("/more")
    public String multi() {
        // 设置队列信息接收回调函数
        rabbitTemplate.setConfirmCallback(myConfirmCallback);
        rabbitTemplate.setReturnCallback(myReturnSMethods);
        // 向交换机发送10条信息
        for (int i = 0; i < 10000; i++) {
            rabbitTemplate.convertAndSend("dirExchange_spring","","你有" + i + "条来自国宝的信息,需要return,请查收");
        }
//        rabbitTemplate.convertAndSend("dirExchange_spring","withRoutingkey","你有一条来自国宝的信息,请查收");
        return "消息已发送,请留意查收!"+ new Date();
    }
    //TTL_dirExchange_spring

    @RequestMapping("/time")
    public String time() {
        // 设置队列信息接收回调函数
//        rabbitTemplate.setConfirmCallback(myConfirmCallback);
//        rabbitTemplate.setReturnCallback(myReturnSMethods);
        // 向交换机发送信息
//        rabbitTemplate.convertAndSend("dirExchange_spring","withRoutingkey","你有一条来自国宝的信息,请查收");
        rabbitTemplate.convertAndSend("TTL_dirExchange_spring","","你有一条来自国宝的信息,限时10秒,请查收");
        return "消息已发送,请留意查收!"+ new Date();
    }


    //发送死信消息
    @RequestMapping("/deal")
    public String deal() {
        rabbitTemplate.setConfirmCallback(myConfirmCallback);
        rabbitTemplate.setReturnCallback(myReturnSMethods);
        // 使用默认交换机发送信息至死信队列
        rabbitTemplate.convertAndSend("deal_queue","这是一次死信队列测试");
        return "消息已发送,请留意查收!"+ new Date();
    }

    //发送死信消息
    @RequestMapping("/short")
    public String contain() {
        rabbitTemplate.setConfirmCallback(myConfirmCallback);
        rabbitTemplate.setReturnCallback(myReturnSMethods);
        // 使用默认交换机发送信息至死信队列
        rabbitTemplate.convertAndSend("deal_queue_short","这是一次死信队列测试");
        return "消息已发送,请留意查收!"+ new Date();
    }

    // deal_queue_short
    // 延时效果实现
    @RequestMapping("/delay")
    public String delay() {
        rabbitTemplate.setConfirmCallback(myConfirmCallback);
        rabbitTemplate.setReturnCallback(myReturnSMethods);
        // 使用默认交换机发送信息至死信队列
        String msg = new Date() + "开始发送要是消息,估计10秒后到达";
        rabbitTemplate.convertAndSend("deal_queue_short",msg);
        return "消息已发送,请留意查收!"+ new Date();
    }

}

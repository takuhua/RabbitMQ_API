package com.itheima;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

/**
 * @Author: taku
 * @CreateTime: 2020-04-27 16:24
 * @Description: 起步引导;类
 */
@SpringBootApplication
public class CallbackApplication {

    public static void main(String[] args) {
        SpringApplication.run(CallbackApplication.class, args);
    }

    // 注入队列
    @Bean
    public Queue dirQueue(){
        return new Queue("dirQueue_spring");
    }
    // 注入交换机,v注意注入的交换机类型
    @Bean
    public DirectExchange dirExchange() {
        return new DirectExchange("dirExchange_spring");
    }

    // 绑定队列与交换机
    @Bean
    public Binding binding(@Qualifier("dirQueue") Queue queue, @Qualifier("dirExchange") DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with("");
//        return BindingBuilder.bind(dirQueue()).to(dirExchange()).with("");

    }

}

package com.itheima.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: taku
 * @CreateTime: 2020-04-27 20:42
 * @Description: 过期队列测试
 */
@Configuration
public class TTLConfig {

    // 过期交换器
    @Bean
    public DirectExchange ttlDirectExchange() {
        return new DirectExchange("TTL_dirExchange_spring");
    }

    // 正常队列
    @Bean
    public Queue normalQueue () {
        return new Queue("normal_queue");
    }

    // 过期队列
    @Bean
    public Queue pastQueue() {
        // "x-message-ttl",10000 设置过期时间为10秒
        return QueueBuilder.durable("TTL_queue").withArgument("x-message-ttl",10000).build();
    }

    // 绑定过期队列和交换机
    @Bean
    public Binding bind() {
        return BindingBuilder.bind(pastQueue()).to(ttlDirectExchange()).with("");
    }

}

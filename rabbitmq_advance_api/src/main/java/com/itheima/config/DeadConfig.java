package com.itheima.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: taku
 * @CreateTime: 2020-04-27 20:42
 * @Description: 死信队列测试
 */
@Configuration
public class DeadConfig {

    // 死信交换器
    @Bean
    public DirectExchange deadDirectExchange() {
        return new DirectExchange("deal_dirExchange_spring");
    }

    // 正常队列
    @Bean
    public Queue liveQueue() {
        return new Queue("live_queue");
    }

    // 死信队列
    @Bean
    public Queue dealQueueshort() {
        // "x-message-ttl",10000 设置过期时间为10秒
        return QueueBuilder.durable("deal_queue_short")
                .withArgument("x-message-ttl", 10000)
                // 设置死信交换机
                .withArgument("x-dead-letter-exchange", "deal_dirExchange_spring")
                // 设置死信rotingkey
                .withArgument("x-dead-letter-routing-key", "log.deal")
                // 设置死信队列容量为1
                .withArgument("x-max-length",1)
                .build();
    }

    // 绑定正常队列和死信交换机
    @Bean
    public Binding dealBind() {
        return BindingBuilder.bind(liveQueue()).to(deadDirectExchange()).with("log.deal");
    }

}

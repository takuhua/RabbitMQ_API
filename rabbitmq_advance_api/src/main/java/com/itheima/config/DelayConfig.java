package com.itheima.config;

import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: taku
 * @CreateTime: 2020-04-27 22:29
 * @Description: t通过ttl实现RabbitMQ的延时管理
 */
@Configuration
public class DelayConfig {

    // 正常队列
    @Autowired
    private Queue normalQueue;
    // 交换机
    DirectExchange ttlDirectExchange;
    // 延时队列
    public Queue pastQueue;

}

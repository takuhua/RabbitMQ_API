package com.itheima.listener;

import com.rabbitmq.client.Channel;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

import java.io.IOException;


/**
 * @Author: taku
 * @CreateTime: 2020-04-27 17:53
 * @Description: 监听器
 */
@Component
public class Mylistener {

    // @RabbitListener(queues = "dirQueue_spring")
    public void shoeMessage(String message) {
        System.out.println("消息已查收,内容是:");
        System.out.println(message);
    }

    //@RabbitListener(queues = "dirQueue_spring")
    public void show(Message message, Channel channel, String msg) {
        long tag = message.getMessageProperties().getDeliveryTag();
        try {
            System.out.println(".....0");
//            int i= 1 / 0;
            System.out.println(msg);
            channel.basicAck(tag,false);
        } catch (Exception e) {
            e.printStackTrace();
            try {
                System.out.println("拒绝处理");
                channel.basicNack(tag,false,false);
            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }


    }
}

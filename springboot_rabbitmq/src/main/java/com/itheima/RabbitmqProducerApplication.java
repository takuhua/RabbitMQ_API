package com.itheima;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @Author: taku
 * @CreateTime: 2020-04-26 21:51
 * @Description: 起步引导类
 */
@SpringBootApplication
public class RabbitmqProducerApplication {
    public static void main(String[] args) {
        SpringApplication.run(RabbitmqProducerApplication.class, args);
    }
}

package com.itheima.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

/**
 * @Author: taku
 * @CreateTime: 2020-04-26 21:53
 * @Description: 消息生产者
 */
@Configuration
public class RabbitConfig {

    // 注入topic 交换机
    @Bean(name = "topic_exchanger_springboot")
    public TopicExchange topicExchanger(){
        return new TopicExchange("topic_exchanger_springboot");
    }

    // 注入direct交换机
    @Bean
    public DirectExchange directExchanger(){
        return new DirectExchange("direct_Exchanger_spring");
    }

    // 注入fanout交换机
    @Bean
    public FanoutExchange fanoutExchangeSpring(){
        return new FanoutExchange("fanout_Exchange_spring");
    }

    // 注入工作队列
    @Bean(name = "direct")
    public Queue directQueue() {
        return QueueBuilder.durable("direct_Queue_Spring").build();
    }

    // 注入工作队列
    @Bean
    public Queue workQueue() {
        return QueueBuilder.durable("work_queue").build();
    }

    @Bean(name = "topic_Queue")
    public Queue topicQueue() {
        return QueueBuilder.durable("topic_Queue").build();
    }

    @Bean
    public Queue fanoutQueuespring() {
        return QueueBuilder.durable("fanout_Queue_spring").build();
    }
//    // 绑定交换机
//    public Binding bindingExchangeTopicQueue() {
//        return BindingBuilder.bind(topicQueue()).to(topicExchanger()).with("log.#");
//    }

    //队列绑定交换机
    @Bean
    public Binding bindingExchangeTopicQueue(@Qualifier("topic_Queue") Queue queue,
                                             // bean的表名用于区分交换机与队列的绑定(别无它用)
                                             @Qualifier("topic_exchanger_springboot")TopicExchange exchange){
        return BindingBuilder.bind(queue).to(exchange).with("log.#");
    }

    //队列绑定交换机
    @Bean
    public Binding bindingExchangeTopicQueue2(){
        return BindingBuilder.bind(fanoutQueuespring()).to(fanoutExchangeSpring());
    }

    //队列绑定交换机,directd交换机的配置不需要route,直接with("")即可
    @Bean
    public Binding bindingExchangeTopicQueue3(@Qualifier("direct")Queue queue){
//        return BindingBuilder.bind(directQueue()).to(directExchanger()).withQueueName();
        return BindingBuilder.bind(queue).to(directExchanger()).with("");
    }


}

package com.itheima.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @Author: taku
 * @CreateTime: 2020-04-26 22:05
 * @Description: springboot整合rabbitMq测试
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class RabbitTest {

    @Autowired
    private RabbitTemplate rabbitTemplate;
    @Autowired
    private DirectExchange directExchanger;

    @Test
    public void producerTest() {

//        rabbitTemplate.convertAndSend("topic_exchanger_springboot","log.info","hell spring_topic");
//        rabbitTemplate.convertAndSend("topic_exchanger_springboot","log.error","hell  error spring_topic");
//        rabbitTemplate.convertAndSend("topic_exchanger_springboot","log.warning","hell warning  spring_topic");
        rabbitTemplate.convertAndSend("fanout_Exchange_spring","log.warning","fanout_Exchange_spring warni");
        rabbitTemplate.convertAndSend("direct_Exchanger_spring","","direct_Exchanger_spring");

    }

}
